const screens = document.querySelectorAll('.screen')
const start_btn = document.getElementById('start-btn')
const choose_btn = document.querySelectorAll('.choose-btn')
const game_container = document.getElementById('game-container')
let selected_pet = {}
let seconds = 0
let score = 0
const timeEl = document.getElementById('time')
const scoreEl = document.getElementById('score')
const message = document.getElementById('message')

start_btn.addEventListener('click', () => screens[0].classList.add('up'))

choose_btn.forEach(btn => {
    btn.addEventListener('click', () => {
        const img = btn.querySelector('img')
        const src = img.getAttribute('src')
        const alt = img.getAttribute('alt')
        selected_pet = {src, alt}
        screens[1].classList.add('up')
        setTimeout(create_pet, 1000)
        start_game()
    });
});

function start_game() {
    setInterval(increase_time, 1000)
}

function increase_time() {
    let m = Math.floor(seconds/60)
    let s = seconds % 60
    m = m < 10 ? `0${m}` : m
    s = s < 10 ? `0${s}` : s
    timeEl.innerHTML = `Time: ${m}:${s}`
    seconds++
}

function create_pet() {
    const pet = document.createElement('div')
    pet.classList.add('pet')
    const {x,y} = getRandomLocation()
    pet.style.top = `${y}px`
    pet.style.left = `${x}px`
    pet.innerHTML = `<img src="${selected_pet.src}" alt="${selected_pet.alt}" style="transform:rotate(${Math.random()*360}deg)">`
    
    pet.addEventListener('click', catch_pet)
    
    game_container.appendChild(pet)
}

function getRandomLocation() {
    const width = window.innerWidth
    const height = window.innerHeight
    const x = Math.random() * (width-200) +100
    const y = Math.random() * (height-200) + 100
    return{x,y}
}

function catch_pet() {
    increase_score()
    this.classList.add('caught')
    setTimeout(() => this.remove(), 2000)
    add_pet()
}

function add_pet() {
    setTimeout(create_pet, 1000)
    setTimeout(create_pet, 1500)
}

function increase_score() {
    score++
    if(score > 19) {
        message.classList.add('visible')
    }
    scoreEl.innerHTML = `Score: ${score}`
}